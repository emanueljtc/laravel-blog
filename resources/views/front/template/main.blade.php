<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>@yield('title', 'Home') | Blog</title>
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/journal/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/general.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">


  </head>
  <body>
      <header>
          @include('front.partials.header')
      </header>
      <div class="container">
          @yield('content')
        <footer>
            <hr>
            Todos los Derechos reservados & copy {{ date('Y') }}
            <div class="pull-right"> Ing. Emanuel Torres</div>
        </footer>
      </div>

        <script src="{{ asset('plugins/jquery/js/jquery-2.1.4.js') }}"></script>
  </body>
</html>
