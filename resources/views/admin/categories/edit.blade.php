@extends('admin.template.main')

@section('title', 'Agregar Categoria' . $category->name)

@section('content')
      {!! Form::open(['route' => ['categories.update', $category], 'method' => 'PUT'])!!}
        <div class="form-group">
          {!! Form::label('name', 'Nombre') !!}
          {!! Form::text('name', $category->name, ['class' => 'form-control', 'placeholder' => 'Nombre de Categoria', 'required']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Actualizar', ['class' => 'bnt btn-primary']) !!}
        </div>

      {!! Form::close() !!}
@endsection
