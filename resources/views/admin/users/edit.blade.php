@extends('admin.template.main')
@section('title', 'Editar Usuario ' . $user->name  )

@section('content')
  {!! Form::open(['method' => 'PATCH', 'action' => ['UsersController@update', $user->id] ])!!}

      <div class="form-group">
          {!! Form::label('name', 'Nombre') !!}
          {!! Form::text('name', $user->name, ['class' => 'form-control', 'placeholder'=>'Nombre Completo',
            'required'] ) !!}
      </div>

      <div class="form-group">
        {!! Form::label('email', 'Correo') !!}
        {!! Form::email('email', $user->email, ['class' => 'form-control', 'placeholder'=>'example@gmail.com', 'required'])!!}
      </div>

        <div class="form-group">
        {!! Form::label('type', 'Tipo') !!}
        {!! Form::select('type', ['member' => 'Miembro', 'admin' => 'Administrador'], $user->type, ['class' => 'form-control'])!!}
      </div>
      <div class="form-group">
          {!! Form::submit('Editar', ['class' => 'bnt btn-primary']) !!}
      </div>

  {!! Form::close() !!}

@endsection
